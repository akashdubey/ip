<!DOCTYPE html>
<html>
<head>
	<title>Online Insurance Portal</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />	
        <link rel="stylesheet" type="text/css" href="css/common_header.css">
	<style type="text/css">		
		#box1{
			float: left;
			display: block;			
			padding: 1%;
			margin: 15% 5% 0 5%;						
			width: 50%;
                        border: solid black;			
		}
                
                #box1 form {
                    margin-left: 25%;
                    text-align: left;                    
                }
                
                #box1 form > label {
                    padding:1%;
                }
                
                #box1 form > input{
                    padding:1%;
                }

		.break {
			clear:both;
		}
	</style>
</head>
<body>
    <% session.setAttribute("username",null); 
       response.setHeader("Pragma","no-cache"); 
       response.setHeader("Cache-Control","no-store"); 
       response.setHeader("Expires","0"); 
       response.setDateHeader("Expires",-1);
       session.invalidate();
    
    %>
 <jsp:include page="menubar.jsp" />
 <div class="container">
     
     <div id="box1">
         <h3> You have been log out from the system </h3>
         
     </div>
 </div>
</body>
</html>