<%@page import="java.sql.ResultSet"%>
<%@page import="mydb.InitDB"%>
<!DOCTYPE html>
<html>
<head>
	<title>Online Insurance Portal</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />	
        <link rel="stylesheet" type="text/css" href="css/common_header.css">
	<style type="text/css">
                
		#box1{                        
			position: relative;
			float: left;
                        display: block;
			padding: 1%;
			margin: 10% 5% 0 2%;						
			width: 65%;
                        border: solid black;
                        

			
		}
                
                
                #box2{                        
			float: left;
			display: block;			
			padding: 1%;			
			width: 100%;                        			
		}
                #box1 form {

                    text-align: left;
                    
                }
                
                #box1 table {
                    border-collapse:collapse;
                    
                }
                
                #box1  tr {

                    
                }
                
                #box1 td {
                    border-top: 1px dashed grey;
                    
                    text-align: left;
                    padding: 10px;
                }
                
                #box1 form > label {
                    
                }
                #box1 form > input{
                    
                }


		.break {
			clear:both;
		}


	</style>
</head>
<body>
 <jsp:include page="menubar.jsp" />	  
 <div class="container">
     
     <div id="box1">
         <form name="newpolicy1" method="get" >
             <table>                  
                   
                   <tr>                 
                     <td> Policy Type </td>
                     <td> 
                         <select name="policytype" onchange="this.form.submit()">
                             <option value="none">none</option>                             
                             <option value="term">Term Policy</option>                             
                             <option value="endowment">Endowment Policy</option>
                             <option value="ulip">Unit Linked Insurance Plan </option>
                         </select>
                     </td>       
                   </tr>
                   
                   <tr>                                            
                     <td> Policy Name</td>
                     <td> Policy Description </td>
                     <td> Yearly Premium</td>
                     <td>Details</td>
                   </tr>
                   <tr>                       
<%          
     String policytype = String.valueOf(request.getParameter("policytype"));      
     String query="select policyname, policywording, entrypremium from policy_inventory where policytype="+"'"+policytype+"'" ;        
     InitDB obj=new InitDB();                  
     ResultSet result= obj.selectQuery(query);          
     while(result.next()){           
         String strpolicyname=result.getString("policyname");
         String strpolicywording=result.getString("policywording");         
         int intentrypremium=Integer.parseInt(result.getString("entrypremium"));
         
%>                                      
                       <td><input id="tmppolicyname" name="tmppolicyname" type="radio" value=<%=strpolicyname %> onchange="capturePolicyName()"  /> <%= strpolicyname.toUpperCase() %></td>
                       <td style='font-size: 0.7em;'><%= strpolicywording.substring(0, 150) %></td>
                       <td><%= intentrypremium %></td>
                       <td><a href="#"> know more</a> </td>
                   </tr>
             
                   
<%   
}
obj.killConnection();
%>

         </table>
         </form>
                
         <form name="newpolicy2" method="get">
             <table>
                    <tr>
                       <input type="hidden" name="policyname" />
                       <td>Proposer</td>
                       <td><input type="text" id="proposer" name="proposer"/></td>
                       
                       <td>Insured</td>
                       <td><input type="text" id="insured" name="insured"/><input type="checkbox" name="sameasproposer" id="sameasproposer"  onclick="IsSameAsProposer()"/>Same as Proposer</td>
                       
                   </tr>
                   
                   <tr>
                       <td>DOB</td>
                       <td><input type="date" name="dob" /></td>
                       
                       <td>Identity Proof ( Only Aadhar/PAN) </td>
                       <td><input type="text" name="idproof" id="idproof" accept="image/*"/></td>
                       
                   </tr>
                   
                   <tr>
                       <td> Insured Amount </td>
                       <td>
                           <input type="number" name="insuredamount" min="100000" max="1000000" />
                           
                       </td>                   
                       <td>Mobile</td>
                       <td><input type="text" name="mobile" maxlength="10"/>
                       
                       
                   </tr>
                   
                   
                                      
                     
                   <tr>
                       <td>Email Addr.</td>
                       <td><input type="email" name="email" maxlength="30"/>
                       <td>Address   </td>
                       <td><input type="text" name="addressproof" /></td>
                       
                   </tr>
                   
                   <tr>
                       <td>State </td>
                       <td>
                           <select name="state">
                               <option value="mp">Madhya Pradesh</option>
                               <option value="cg">Chattisgarh</option>
                           </select>
                       </td>
                       <td>District </td>
                       <td>
                           <select name="district">
                               <option value="jabalpur">Jabalpur</option>
                               <option value="raipur">Raipur</option>
                           </select>
                       </td>
                                              
                   </tr>
                   
                   
                   <tr>
                       <td>Pin code </td>
                       <td>
                           <select name="pin">
                               <option value="482001">482001</option>
                               <option value="482002">482002</option>
                               <option value="482003">482003</option>
                               <option value="482004">482004</option>                               
                           </select>
                       </td>
                       <td>Country</td>
                       <td><input type="text" name="country" value="india" readonly="readonly"</td>                       
                   </tr>
                                                         
                       <tr id="box2">
                           <td style="border:none">
                               <input type="submit" name="next" id="next" value="Next" style="width: 15em;height:3em; text-align: center;" onclick="verifyPageControls()"/>
                           </td>
                       </tr>                                  
             </table>
         </form>
     </div>
 </div>
</body>


    <script>
        var tmp;
        function IsSameAsProposer() {
         var person =document.getElementById("proposer").value;             
         if (document.getElementById("sameasproposer").checked == true){
                document.getElementById("insured").value= person;
         } else {
            document.getElementById("insured").value="";
           }                             
       }
        function verifyPageControls(){
            
        }
        function capturePolicyName(){            
            var frm=document.forms["newpolicy2"];
            var elmnts=frm.elements["policyname"];
            elmnts.value= document.querySelector('input[name = "tmppolicyname"]:checked').value;
            // we no longer the alert, we now know for the fact that this function works :-)
            // alert(elmnts.value);
        }
        
        
    </script>
        
    <% 
              String btnnext=String.valueOf(request.getParameter("policyname"));                  
                if(!btnnext.equalsIgnoreCase("null")){
                    session.setAttribute("selectedpolicyname", btnnext);
                    session.setAttribute("dob", request.getParameter("dob"));
                    session.setAttribute("proposer", request.getParameter("proposer"));
                    session.setAttribute("insured" , request.getParameter("insured"));
                    session.setAttribute("idproof", request.getParameter("idproof"));         
                    session.setAttribute("insuredamount" , request.getParameter("insuredamount"));
                    session.setAttribute("mobile", request.getParameter("mobile"));
                    session.setAttribute("email", request.getParameter("email"));
                    session.setAttribute("addressproof" , request.getParameter("addressproof"));
                    session.setAttribute("state" , request.getParameter("state"));
                    session.setAttribute("district", request.getParameter("district"));
                    session.setAttribute("pin", request.getParameter("pin"));
                    session.setAttribute("country", request.getParameter("country"));
                    response.sendRedirect("newpolicy_process.jsp");
                } else{
                    out.println("tmppolicyname : "+ btnnext);
                }
    %>

          <%
    if (session.getAttribute("username") == null){        
        response.sendRedirect("./login.jsp");
    }else if(String.valueOf(session.getAttribute("username")).equals("admin")){
        response.sendRedirect("logout.jsp");
    }
    
    %>

</html>