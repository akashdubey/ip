<!DOCTYPE html>
<html>
<head>
	<title>Online Insurance Portal</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />	
        <link rel="stylesheet" type="text/css" href="css/common_header.css">
	<style type="text/css">
		
		#box1{
			float: left;
			display: block;			
			padding: 1%;
			margin: 15% 5% 0 5%;						
			width: 50%;
                        border: solid black;

			
		}
                
                #box1 form {
                    margin-left: 25%;
                    text-align: left;
                    
                }
                
                #box1 form > label {
                    padding:1%;
                }
                #box1 form > input{
                    padding:1%;
                }


		.break {
			clear:both;
		}


	</style>
</head>
<body>
    <jsp:include page="menubar.jsp" />	    
 <div class="container">
     
     <div id="box1">
         <form name="login" method="get" action="login_process.jsp">
             <table>
                   <tr>                 
                     <td> <label for="usr"> User Name </label></td>
                     <td><input type="text" name="user" /></td>
                   </tr>
                   
                   <tr>
                     <td><label for="ps"> Password </label></td>
                     <td><input type="password" name="password" /></td>
                   </tr>                     
                   
                   <tr>
                       <td>User Type</td>
                       <td>
                           <select name="utype">
                               <option >Agency</option>
                               <option >Customer</option>
                           </select>
                       </td>
                     <td><input type="submit" name="submit" value="Login"/></td>
                   </tr>                     
                   
                   <tr>
                       <td></td>
                       <td>New user ? <a href="./signup.jsp">signup here</a></td>
                   </tr>                                                                                                                         
             </table>
         </form>
     </div>
 </div>
</body>
</html>