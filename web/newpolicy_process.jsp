
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.*" %>
<%@page import="mydb.InitDB" %>
<%@page import="java.sql.ResultSet"%>
<%@page import="mydb.InitDB"%>
<head>
	<title>Online Insurance Portal</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />	
        <link rel="stylesheet" type="text/css" href="css/common_header.css">
	<style type="text/css">
		
		#box1{
			float: left;
			display: block;			
			padding: 1%;
			margin: 15% 5% 0 5%;			
			width: 50%;
                        border: solid black;

			
		}
                
                #box1 form {
                    margin-left: 25%;
                    text-align: left;
                    
                }
                
                #box1 form > label {
                    
                }
                #box1 form > input{
                    
                }


		.break {
			clear:both;
		}

	</style>
</head>
<body>
<jsp:include page="menubar.jsp" />	   
 
 <div class="container">
     
     <div id="box1">
       
       <% 
           
           String strpolicyname=String.valueOf(session.getAttribute("selectedpolicyname"));                                            
           String strdate=String.valueOf(session.getAttribute("dob"));                      
           Date dtdob=new SimpleDateFormat("yyyy-MM-dd" , Locale.getDefault()).parse(strdate);           
           java.sql.Date dob=new java.sql.Date(dtdob.getTime());
           java.sql.Date purchasedate=new java.sql.Date(System.currentTimeMillis());
           String strproposer=String.valueOf(session.getAttribute("proposer"));
           String strinsured=String.valueOf(session.getAttribute("insured"));
           String stridproof=String.valueOf(session.getAttribute("idproof"));         
           int intinsuredamount=Integer.parseInt(String.valueOf(session.getAttribute("insuredamount")));
           String strmobile=String.valueOf(session.getAttribute("mobile"));
           String stremail=String.valueOf(session.getAttribute("email"));
           String straddressproof=String.valueOf(session.getAttribute("addressproof"));
           String strstate=String.valueOf(session.getAttribute("state"));
           String strdistrict=String.valueOf(session.getAttribute("district"));
           String strpin=String.valueOf(session.getAttribute("pin"));
           String strcountry=String.valueOf(session.getAttribute("country"));
           
         double tmppremium=0;
         InitDB obj=new InitDB();   
         //if (strupdate != null){
         //String query="update policy_inventory set policytype="+"'"+strpolicytype+"'"+", policyname="+"'"+strpolicyname+"'"+", policywording="+"'"+strpolicywording+"'"+", entryage="+intentryage+", entrypremium="+intentrypremium+" where policyname="+"'"+session.getAttribute("selectedpolicyname")+"'" ;
         
         String query="select entrypremium from policy_inventory where policyname="+"'"+strpolicyname+"'";         
         ResultSet result= obj.selectQuery(query);
         while (result.next()){
             tmppremium=Integer.parseInt(result.getString("entrypremium"));
         }result.close();
         
         // borrowed from internet ;-), coz we don't reinvent the wheel all the time //
            int yearDOB = Integer.parseInt(strdate.substring(0, 4));
            int monthDOB = Integer.parseInt(strdate.substring(5, 7));
            int dayDOB = Integer.parseInt(strdate.substring(8, 10));
            DateFormat dateFormat = new SimpleDateFormat("yyyy");
            java.util.Date date = new java.util.Date();
            int thisYear = Integer.parseInt(dateFormat.format(date));

            dateFormat = new SimpleDateFormat("MM");
            date = new java.util.Date();
            int thisMonth = Integer.parseInt(dateFormat.format(date));

            dateFormat = new SimpleDateFormat("dd");
            date = new java.util.Date();
            int thisDay = Integer.parseInt(dateFormat.format(date));

            int age = thisYear - yearDOB;


            if (thisMonth < monthDOB) {
                age = age - 1; 
        }

          if (thisMonth == monthDOB && thisDay < dayDOB) {
                age = age - 1;
        }
          if(age >=18 && age <=24){
              tmppremium=tmppremium+(tmppremium*1/100);
          }
          else if (age >=25 && age <=35) {
            tmppremium=tmppremium+(tmppremium*20/100);
          }else if (age >=36 && age <=45){
              tmppremium=tmppremium+(tmppremium*30/100);
          }else if (age >=46 && age <=60){
              tmppremium=tmppremium+(tmppremium*60/100);
          }else {
              
              tmppremium=tmppremium+(tmppremium*100/100);
          }                  

         
          
          
          query="insert into user_inventory (policyname, username, proposer, insured, dob, insuredamount, premium, " 
                 + "mobile,email,addressproof,idproof,state,district,pin,country,purchasedate) values ("+ "'"+strpolicyname+"',"
                 + "(select username from registration where username="+"'"+session.getAttribute("username")+"'" + ")"
                 + ","+"'"+strproposer+"', "+"'"+strinsured+"', "+"'"+dob+"', "+"'"+intinsuredamount+"', "+"'"+tmppremium+"', "
                 + "'"+strmobile+"', "+"'"+stremail+"'"+", " +"'"+straddressproof+"', "+"'"+stridproof+"', "
                 + "'"+strstate+"', "+ "'"+ strdistrict + "', " + "'"+ strpin + "', "+ "'" + strcountry 
                 +"','" + purchasedate + "')";

          
          
          boolean status=obj.insertQuery(query);
                  
          if (status == true){
                out.println("Thanks requested information stored successfully" );
                response.sendRedirect("payment.jsp");
          }
            else{
                out.println("An Error occured during insert" );
                
                out.println(query);
          }
                
          session.setAttribute("payment", tmppremium);
obj.killConnection();
%>
     </div>
 
 

          <%
    if (session.getAttribute("username") == null){       
        response.sendRedirect("./login.jsp");
    }
    
    %>


</body>
</html>