<html>
<head>
	<title>Online Insurance Portal</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />	
        <link rel="stylesheet" type="text/css" href="css/common_header.css">
	<style type="text/css">
                
		#box1{
                        position: relative;
			float: left;
			display: block;			
			padding: 1%;
			margin: 10% 5% 0 2%;			
			width: 65%;
                        border: solid black;

			
		}
                
                
                #box2{                        
			float: left;
			display: block;			
			padding: 1%;
			width: 100%;                        			
		}
                #box1 form {

                    text-align: left;
                    
                }
                
                #box1 table {
                    border-collapse:collapse;
                    
                }
                
                #box1  tr {

                    
                }
                
                #box1 td {
                    border-top: 1px dashed grey;
                    
                    text-align: left;
                    padding: 10px;
                }
                
                #box1 form > label {
                    
                }
                #box1 form > input{
                    
                }


		.break {
			clear:both;
		}


	</style>
</head>
<body>
    <jsp:include page="menubar.jsp" />	  
    <div class="container">
     
     <div id="box1">
         <form name="wallet" method="post" >
             
             <table>
                    
                   <tr> Wallet
                       <td></td>                                          
                       <td>                                                       
                               <input type="radio" name="paytm" value="paytm">Paytm</option>
                       </td>                              
                       <td>                                                       
                               <input type="radio" name="freecharge" value="freecharge">FreeCharge</option>
                       </td>                         
                       <td>                                                       
                               <input type="radio" name="jiomoney" value="jiomoney">Jio Money</option>                               
                       </td>                              
                                                                     
                   </tr>                   
                   
                   <tr>
                       <td>Mobile Number</td>                      
                       <td></td>
                       <td></td>
                       <td><input type="text" maxlength="10" size="17"></td>                         
                   </tr>                   
                   
                   <tr>
                       <td>Password</td>                       
                       <td></td>
                       <td></td>
                       <td><input name="wallet_password" type="password" maxlength="10" size="17"></td>                         
                       
                   </tr>                   
                   
                   
                   <tr>
                       <td>Amount</td>
                       <td></td>
                       <td></td>
                       <td><input type="text" name="wallet_amount" maxlength="5" size="17" readonly="readonly" value=<%= String.valueOf(session.getAttribute("payment"))%> ></td>                       
                   </tr>                   
                       <tr id="box2">
                         <td style="border:none">
                             <input type="submit" name="wallet_next" value="Pay Now" style="width: 15em;height:3em; text-align: center;" >
                         </td>
                         
                       </tr>                                                                                                                                                                                              
             </table>
         </form>
     </div>
 </div>
</body>
    

   <%
    if (session.getAttribute("username") == null){
        response.sendRedirect("./login.jsp");
    }                

    if (request.getParameter("wallet_next")!= null){
        response.sendRedirect("success_payment.jsp");
    }
    %>

</html>

