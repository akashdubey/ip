<!DOCTYPE html>
<html>
<head>
	<title>Online Insurance Portal</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />	
        <link rel="stylesheet" type="text/css" href="css/common_header.css">
	<style type="text/css">
                
		#box1{
                        position: relative;
			float: left;
			display: block;			
			padding: 1%;
			margin: 10% 5% 0 2%;			
			width: 65%;
                        border: solid black;			
		}
                
                #box1 li{
                        list-style-type: none;
                        text-align: right;
                }
                
                
                #box2{                        
			float: left;
			display: block;			
			padding: 1%;
			width: 100%;                        			
		}
                #box1 form {

                    text-align: left;
                    
                }
                
                #box1 table {
                    border-collapse:collapse;
                    
                }
                
                #box1  tr {

                    
                }
                
                #box1 td {
                    border-top: 1px dashed grey;
                    
                    text-align: left;
                    padding: 10px;
                }
                
                #box1 form > label {
                    
                }
                #box1 form > input{
                    
                }


		.break {
			clear:both;
		}


	</style>
</head>
<body>
      <jsp:include page="menubar.jsp" />	  
 <div class="container">
     
  <div id="box1">       
      
      <h3> Current User : <%= String.valueOf(session.getAttribute("username")) %></h3>
      <h3> Premium Amt : <%= String.valueOf(session.getAttribute("payment")) %></h3>
          <table>
               
              <tr>
                  <td> Please click on appropriate link to proceed with payment </td>                  
              </tr>
              
              <tr>
                  <td> 
                      <ul>
                          <li><a href="./credit-debit_card.jsp">Credit/Debit Card</a></li>
                          <li><a href="./netbanking.jsp">Internet Banking</a></li>
                          <li><a href="./wallet.jsp">Wallet</a></li>
                   
                      </ul> 
                  </td>                  
              </tr>
          </table>
      
  </div>
     
      
             <%
    if (session.getAttribute("username") == null){       
        response.sendRedirect("./login.jsp");
    }
    
    %>
       
 </div>
</body>
</html>