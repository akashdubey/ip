<%@page import="java.sql.ResultSet"%>
<%@page import="mydb.InitDB"%>
<!DOCTYPE html>
<html>
<head>
	<title>Online Insurance Portal</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />	
        <link rel="stylesheet" type="text/css" href="css/common_header.css">
        <style type="text/css">
                
		#box1{                        
			position: relative;
			float: left;
                        display: block;
			padding: 1%;
			margin: 10% 5% 0 2%;						
			width: 65%;
                        border: solid black;                        			
		}
                
                
                #box2{                        
			float: left;
			display: block;			
			padding: 1%;			
			width: 100%;                        			
		}
                #box1 form {
                    text-align: left;                    
                }
                
                #box1 table {
                    border-collapse:collapse;                    
                }
                
                #box1  tr {                   
                }
                
                #box1 td {
                    border-top: 1px dashed grey;                    
                    text-align: left;
                    padding: 10px;
                }
                
                #box1 form > label {                    
                }
                #box1 form > input{                    
                }


		.break {
			clear:both;
		}
	</style>
</head>
<body>
<jsp:include page="menubar.jsp" />	  

<div class="container">
     
     <div id="box1">
         
         <form method="post" action="policyinventory_edit_process.jsp">
         <table>                  

<%
    String query="select policytype, policyname, policywording, entryage, entrypremium from policy_inventory where policyname="+ "'"+session.getAttribute("selectedpolicyname")+ "'";        
     InitDB obj=new InitDB();                  
     ResultSet result= obj.selectQuery(query);
     while(result.next()){  
         String strpolicytype=result.getString("policytype");
         String strpolicyname=result.getString("policyname");
         String strpolicywording=result.getString("policywording");
         int intentryage=(result.getInt("entryage"));
         int intentrypremium=(result.getInt("entrypremium"));
%>
        
                    <tr>
                       <td> Policy Type</td>
                       <td>
                           <input type="text" name="policytype" value=<%= strpolicytype %> />
                       </td>                   
                   </tr>

                   <tr>
                       <td> Policy Name </td>
                       <td>
                           <input type="text" name="policyname" value=<%= strpolicyname %> />
                       </td>                   
                   </tr>
                   <tr>
                       <td> Policy wording </td>
                       <td>
                           <textarea rows="10" cols="80" name="policywording" wrap="hard" ><%= strpolicywording %></textarea>
                       </td>                   
                   </tr>
                   
                   <tr>
                       <td> Minimum Entry Age </td>
                       <td>
                           <input type="number" name="entryage" min="1" max="45" value=<%= intentryage %> />
                       </td>                   
                   </tr>
                   
                   <tr>
                       <td> Minimum Premium Per Lakh</td>
                       <td>
                           <input type="number" name="entrypremium" min="1000" max="20000" value=<%= intentrypremium %> />
                       </td>                   
                   </tr>
                   <tr id="box2">
                           <td style="border:none">
                               <input type="submit" name="update" value="Update" style="width: 15em;height:3em; text-align: center;"/>                               
                           </td>                           
                       </tr>                       
                       <tr id="box2">
                           <td style="border:none">
                               <input type="submit" name="cancel" value="Cancel" style="width: 15em;height:3em; text-align: center; "/>
                               
                           </td>
                       </tr>
         </table>         
                       
         </form>
     </div>
<%
    }
 result.close();
 obj.killConnection();
 
%>
      <%
    if (session.getAttribute("username") == null){
        response.sendRedirect("./login.jsp");
    }
    
    %>
</body></html>
