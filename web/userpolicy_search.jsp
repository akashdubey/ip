<%@page import="java.lang.String"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="mydb.InitDB"%>
<!DOCTYPE html>
<html>
<head>
    
	<title>Online Insurance Portal</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />	
        <link rel="stylesheet" type="text/css" href="css/common_header.css">
	<style type="text/css">
                
		#box1{
                        position: relative;
			float: left;
			display: block;			
			padding: 1%;
			margin: 10% 5% 0 2%;			
			width: 65%;
                        border: 6px solid black;			
		}
                
                
                #box2{                        
			float: left;
			display: block;			
			padding: 1%;
			width: 100%;                        			
		}
                
                #box1 form {
                    text-align: left;                    
                }
                
                #box1 table {
                    border-collapse:collapse;                    
                }
                
                #box1 th {
                border: 1px dashed grey;                    
                    text-align: center;
                    padding: 10px;    
                }                                
                
                #box1 td {                    
                    text-align: left;
                    padding: 10px;
                }                                
                
                
                #box3{
                        position: relative;
			float: left;
			display: block;			
			padding: 1%;
			margin: 1% 5% 0 2%;			
			width: 65%;
                        border: solid black;	                        
		}
                
                #box3 form {
                    text-align: left;                    
                }
                
                #box3 table {
                    border-collapse:collapse;                    
                }
                                                
                #box3 td {
                    border-top: 1px dashed grey;                    
                    text-align: left;
                    padding: 10px;
                }                                
                


		.break {
			clear:both;
		}


	</style>
</head>
<body>
 <jsp:include page="menubar.jsp" />	  
 <div class="container">
     
     <div id="box1">
         <h3>Search by :</h3>
         <form name="searchpolicy1" method="post" action="userpolicy_edit.jsp">
             <table>
                 <tr>
                     <td> Field: 
                      <select name="searchby" >
                        <option value="username">username</option>
                        <option value="mobile">mobile</option>
                        <option value="email">email</option>
                      </select>
                     </td>             
                     <td><input type="text" name="searchvalue" /> </td>
                     <td><input type="submit" name="search" value="search" /> </td>
                 </tr>                 
             </table>                                                       
         </form>
     </div>
 </div>
</body>        
   <%                            
    if (session.getAttribute("username") == null ){
        response.sendRedirect("./login.jsp");
    }else if (! String.valueOf(session.getAttribute("username")).equalsIgnoreCase("admin")){
        response.sendRedirect("./logout.jsp");
    }
    
    %>
    
</html>