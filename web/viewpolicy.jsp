<%@page import="java.sql.ResultSet"%>
<%@page import="mydb.InitDB"%>
<!DOCTYPE html>
<html>
<head>
	<title>Online Insurance Portal</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />	
        <link rel="stylesheet" type="text/css" href="css/common_header.css">
	<style type="text/css">
                
		#box1{
                        position: relative;
			float: left;
			display: block;			
			padding: 1%;
			margin: 10% 5% 0 2%;			
			width: 65%;
                        border: solid black;			
		}
                
                
                #box2{                        
			float: left;
			display: block;			
			padding: 1%;
			width: 100%;                        			
		}
                
                #box1 form {
                    text-align: left;                    
                }
                
                #box1 table {
                    border-collapse:collapse;                    
                }
                
                #box1 th {
                    border: 1px dashed grey;                    
                    text-align: center;
                    padding: 10px;
                }                                
                                                
                #box1 td {
                    border: 1px dashed grey;                    
                    text-align: center;
                    padding: 10px;
                }                                
                
                #box3{
                        position: relative;
			float: left;
			display: block;			
			padding: 1%;
			margin: 1% 5% 0 2%;			
			width: 65%;
                        border: solid black;	                        
		}
                
                #box3 form {
                    text-align: left;                    
                }
                
                #box3 table {
                    border-collapse:collapse;                    
                }
                                                
                #box3 td {
                    border-top: 1px dashed grey;                    
                    text-align: left;
                    padding: 10px;
                }                                
                


		.break {
			clear:both;
		}


	</style>
</head>
<body>
 <jsp:include page="menubar.jsp" />	  
 <div class="container">
     
     <div id="box1">
         <h3>List of currently enrolled policies</h3>
         <form name="viewpolicy" >
             <table>                                  
                 
                     <th> Policy Name</th>
                     <th> Insured Person</th>
                     <th> Insurance Cover</th>
                     <th> Premium payable</th>
                     <th> Date of Purchase</th>                                                                    
      <%

         
         String query="select policyname,insured, insuredamount, premium, purchasedate from user_inventory where username="+"'"+String.valueOf(session.getAttribute("username"))+"'" ;        
         
         InitDB obj=new InitDB();                  
         ResultSet result= obj.selectQuery(query);          
      
         while(result.next()){           
           String strpolicyname=result.getString("policyname");
           String strinsured=result.getString("insured");         
           String strinsuredamount=result.getString("insuredamount");         
           String strpremium=result.getString("premium");         
           String strpurchasedate=result.getString("purchasedate");         
         
       %>
                                          
                     <tr>
                         <td><%= strpolicyname.toUpperCase() %></td>
                         <td><%= strinsured.toUpperCase() %></td>
                         <td><%= strinsuredamount %></td>
                         <td><%= strpremium %></td>
                         <td><%= strpurchasedate %></td>                                                                           
                     </tr>
                     
          <%  
            }
            obj.killConnection();
          %>
          
            
             </table>
         </form>
     </div>
     <div class="break"></div>
     
 </div>
</body>
   <%
    if (session.getAttribute("username") == null){
        response.sendRedirect("./login.jsp");
    }
    
    %>
</html>