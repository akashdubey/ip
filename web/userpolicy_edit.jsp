<%@page import="java.lang.String"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="mydb.InitDB"%>
<!DOCTYPE html>
<html>
<head>
    
	<title>Online Insurance Portal</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />	
        <link rel="stylesheet" type="text/css" href="css/common_header.css">
	<style type="text/css">
                
		#box1{
                        position: relative;
			float: left;
			display: block;			
			padding: 1%;
			margin: 10% 5% 0 2%;			
			width: 65%;
                        border: solid black;			
		}
                
                
                #box2{                        
			float: left;
			display: block;			
			padding: 1%;
			width: 100%;                        			
		}
                
                #box1 form {
                    text-align: left;                    
                }
                
                #box1 table {
                    border-collapse:collapse;                    
                }
                
                #box1 th {
                    border: 1px dashed grey;                    
                    text-align: center;
                    padding: 10px;
                }                                
                
                #box1 td {
                    border: 1px dashed grey;                    
                    text-align: left;
                    padding: 10px;
                }                                
                
                
                #box3{
                        position: relative;
			float: left;
			display: block;			
			padding: 1%;
			margin: 1% 5% 0 2%;			
			width: 65%;
                        border: solid black;	                        
		}
                
                #box3 form {
                    text-align: left;                    
                }
                
                #box3 table {
                    border-collapse:collapse;                    
                }
                                                
                #box3 td {
                    border-top: 1px dashed grey;                    
                    text-align: left;
                    padding: 10px;
                }                                
                


		.break {
			clear:both;
		}

	</style>
</head>
<body>
 <jsp:include page="menubar.jsp" />	  
 <%
           String strsearchby=request.getParameter("searchby");     
           String strsearchvalue=request.getParameter("searchvalue");
    	   String streditpolicyname="" ;
	   String streditproposer="";
           String streditinsured="";
           String streditinsuredamount="";
           String streditdate="";
           String streditmobile="";
           String streditemail="";
           String streditaddressproof="";
           String streditidproof="";
 %>
 
 <div class="container">     
     <div id="box1">       
         <h3 id ="textsearchby">Search by :</h3>
         <form id="searchpolicy2" name="searchpolicy2" method="get">             
             <input type="hidden" id="searchby" name="searchby" value=<%=strsearchby %> >
             <input type="hidden" id="searchvalue" name="searchvalue" value=<%=strsearchvalue %> >
             <table>                                                  
                     <th> Policy Name</th>
                     <th> Insured Person</th>
                     <th> Insurance Cover</th>
                     <th> Premium payable</th>
                     <th> Date of Purchase</th>                                                                                           
      <%            
         
         if(request.getParameter("searchby")!= null && request.getParameter("searchvalue")!=null) {                                
           InitDB obj=new InitDB();                                    
           String query1="select * from user_inventory where "+ strsearchby+"="+"'"+strsearchvalue+"'";
           ResultSet result= obj.selectQuery(query1);              
           while(result.next()){           
             String strpolicyname=result.getString("policyname");
             String strinsured=result.getString("insured");         
             String strinsuredamount=result.getString("insuredamount");         
             String strpremium=result.getString("premium");         
             String strpurchasedate=result.getString("purchasedate");         
             
   
       %>                                          
                     <tr>
                         <td><input type="radio" name="policyname" value=<%= strpolicyname.toUpperCase() %> onchange="this.form.submit()"/><%= strpolicyname.toUpperCase() %></td>                         
                         <td><%= strinsured.toUpperCase() %></td>
                         <td><%= strinsuredamount %></td>
                         <td><%= strpremium %></td>
                         <td><%= strpurchasedate %></td>                                                                                                    
                     </tr>                     
                     
          <%             
            }
               result.close();                           
                if ( request.getParameter("policyname")!= null){        
                    session.setAttribute("selectedpolicyname",request.getParameter("policyname"));                    %>
                    <script language="javascript">
                        document.getElementById("searchpolicy2").style.display="none";                                                                         
                        document.getElementById("textsearchby").style.display="none"
                    </script>
             </table>
         </form>       
                    <%
                        String query2=query1+" and policyname=" + "'"+session.getAttribute("selectedpolicyname")+"'";     
                        
                        result= obj.selectQuery(query2);              
                        while(result.next()){                                   
                             streditpolicyname = result.getString("policyname");
                             streditproposer = result.getString("proposer");
                             streditinsured = result.getString("insured");
                             streditinsuredamount = result.getString("insuredamount");
                             streditdate=result.getString("dob");                             
                             streditmobile=result.getString("mobile");
                             streditemail=result.getString("email");
                             streditaddressproof=result.getString("addressproof");
                             streditidproof=result.getString("idproof");                             
                        }
                 %>                 
                 <form id="editpolicy" name="editpolicy" method="get">
                    <input type="hidden" id="searchby" name="searchby" value=<%=strsearchby %> >
                    <input type="hidden" id="searchvalue" name="searchvalue" value=<%=strsearchvalue %> >
                     <table>
                      <tr>
                       <input type="hidden" name="policyname" value=<%=streditpolicyname %> />
                       <td>Proposer</td>
                       <td><input type="text" id="proposer" name="proposer" value=<%=streditproposer %> /></td>                       
                       <td>Insured</td>
                       <td><input type="text" id="insured" name="insured" value=<%=streditinsured %> /><input type="checkbox" name="sameasproposer" id="sameasproposer"  onclick="IsSameAsProposer()"/>Same as Proposer</td>                       
                      </tr>                   
                      <tr>
                       <td>DOB</td>
                       <td><input type="date" name="dob" value=<%=streditdate %> readonly="readonly" /></td>                       
                       <td>Identity Proof ( Only Aadhar/PAN) </td>
                       <td><input type="text" name="idproof" id="idproof" value=<%=streditidproof %> /></td>                       
                     </tr>                  
                     <tr>
                       <td> Insured Amount </td>
                       <td>
                           <input type="number" name="insuredamount" value=<%=streditinsuredamount %> readonly="readonly" />                           
                       </td>                   
                       <td>Mobile</td>
                       <td><input type="text" name="mobile" maxlength="10" value=<%=streditmobile %> />                       
                     </tr>                     
                     <tr>
                       <td>Email Addr.</td>
                       <td><input type="email" name="email" maxlength="30" value=<%=streditemail %> />
                       <td>Address   </td>
                       <td><input type="text" name="addressproof" value=<%=streditaddressproof %> /></td>                       
                     </tr>                   
                     <tr>
                       <td>State </td>
                       <td>
                           <select name="state">
                               <option value="mp">Madhya Pradesh</option>
                               <option value="cg">Chattisgarh</option>
                           </select>
                       </td>
                       <td>District </td>
                       <td>
                           <select name="district">
                               <option value="jabalpur">Jabalpur</option>
                               <option value="raipur">Raipur</option>
                           </select>
                       </td>
                                              
                   </tr>                                      
                   <tr>
                       <td>Pin code </td>
                       <td>
                           <select name="pin">
                               <option value="482001">482001</option>
                               <option value="482002">482002</option>
                               <option value="482003">482003</option>
                               <option value="482004">482004</option>                               
                           </select>
                       </td>
                       <td>Country</td>
                       <td><input type="text" name="country" value="india" readonly="readonly"</td>                       
                   </tr>
                   <tr id="box2">
                           <td style="border:none">
                               <input type="submit" name="update" id="update" value="Update" style="width: 15em;height:3em; text-align: center;" onclick="this.form.submit()"/>
                           </td>
                  </tr>                                  
             </table>
    </form> 
         
                       <script language="javascript">                        
                        document.getElementById("editpolicy").style.display="block";                                                 
                    </script>

                 
                 <%                         
                }
                String strupdate=request.getParameter("update");                 
                if (strupdate != null){
                    String query3="update user_inventory set proposer="+"'"+request.getParameter("proposer")+"'"
                    +", insured="+"'"+request.getParameter("insured")+"'"
                    +", mobile="+"'"+request.getParameter("mobile")+"'"
                    +", email="+"'"+request.getParameter("email")+"'"
                    +", addressproof="+"'"+request.getParameter("addressproof")+"'"
                    +", idproof="+"'"+request.getParameter("idproof")+"'"
                    +", state="+"'"+request.getParameter("state")+"'"
                    +", district="+"'"+request.getParameter("district")+"'"
                    +", pin="+"'"+request.getParameter("pin")+"'"
                    +" where " +strsearchby+ "="+"'"+strsearchvalue+"'";
                    boolean status = obj.insertQuery(query3);
                    if (status == true){
                        response.sendRedirect("success_user_edit.jsp");
                    } else {
                        out.println("<h3>Update failed, Please contact application administrator</h3>" );                        
                      }
              obj.killConnection();                
              
            }

}     
          %>                      
                   
          
     </div>
     <div class="break"></div>
     
 </div>
</body>

<script>
    
    
    
</script>
    
   <%             
    if (session.getAttribute("username") == null ){
        response.sendRedirect("./login.jsp");
    } else if (! String.valueOf(session.getAttribute("username")).equalsIgnoreCase("admin")){
        response.sendRedirect("./logout.jsp");
      }    
   %>
</html>