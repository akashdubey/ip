<%@page import="java.util.*" %>
<!DOCTYPE html>
<html>
<head>
	<title>Online Insurance Portal</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />	
        <link rel="stylesheet" type="text/css" href="css/common_header.css">
	<style type="text/css">
		
		#box1{
			float: left;
			display: block;			
			padding: 0;
			margin: 10% 5% 0 2%;			
			width: 25%;			
		}

                #box2{
			float: left;
			display: block;			
			padding: 0;
			margin: 10% 2% 0 2%;						
			width: 45%;			
                        height: 20%;
                        
		}
		#box3{
                        position: absolute;
                        top: 100%;
			float: left;
			display: block;			
			padding: 0;
			margin: 10% 2% 0 5%;						
			width: 40%;                        
		}
                
                #box3 img {
                    margin: 10%;
                }

		.break {
			clear:both;
		}


	</style>
</head>
<body>
        
<jsp:include page="menubar.jsp" />	  
 
 <div id="break" > </div>
 <div class="container">
  <div id="box2">
      <h3 style=" padding :5px; color: #591a30; text-align: center;"> What's new</h3>
      <img src="./image/buffet.jpg" align="top" width="550px"  height="400px" style="margin-left: 5%;"/>
      <p style=" padding :5px; color: #591a30; font-size: 0.8em;"><i>Buffett has said that for the longest time interest rates acted like gravity and if interest rates go higher it will have a impact on return ratios. 
      <a href="http://economictimes.indiatimes.com/articleshow/58587103.cms?utm_source=contentofinterest&utm_medium=text&utm_campaign=cppst">Read more here</a></i></p>      
  </div>
     
  <div id="box2">
      <h3 style=" padding :5px; color: #591a30; text-align: center;"> Insurance is most-searched financial product</h3>
      <img src="./image/google.jpg" align="top" width="550px"  height="400px" style="margin-left: 5%;"/>
      <p style=" padding :5px; color: #591a30; font-size: 0.8em; text-align: justify;">SINGAPORE Eight out of the top 10 financial searches in Singapore over the last year were about insurance policies ranging from health and business to housing and cars, revealed Google on Monday.

Yesterday also saw the search engine launch its Asia-Pacific (Apac) financial dashboard, sharing details on how consumers in 10 areas across the region ? Singapore, Australia, Hong Kong, Indonesia, India, Japan, Malaysia, Philippines, Thailand, Vietnam ? conducted searches for financial products over the last year.

Some of the top financial Google searches in Singapore include: ?How to make car insurance cheaper?, ?Why life insurance is important for future success? as well as ?How to avoid pitfalls in getting home equity loans?. The tool takes into account all the different languages.

?The topic of insurance is very heavy in Singapore,? said Mr Michael Yue, industry head for banking and financial services, Google Singapore.

?Financial literacy in Singapore is one of the highest in Asia and consumers like to be well-informed before making any decisions. Singaporeans care about financial planning and often turn to (Google) Search to help them understand financial products and investments.?

With smartphone penetration rate at 91 per cent, Singaporeans have easy access to the Internet and have made it a one-stop platform to gather information and plan their finances, noted Google. Mobile search growth for financial products is increasing by 28 per cent year-on-year, it added.

For example, using the mobile to send a search related to ?investing? in Singapore drew an approximate query volume of 70 million for last year, compared to Hong Kong?s 40 million query volume, information from the APAC financial dashboard showed. Meanwhile a search related to ?general banking? on Google doubled in mobile search volume for Singapore at 40 million, compared to Hong Kong?s 20 million.

Mr Yue noted that many organisations have been making ?incredible shifts? to digital services in recent years. ?They understand that?s where the users are really spending their time. So when you look across all the major local banks, DBS, OCBC, UOB, they are investing quite heavily in digital channels,? he said.

?I think it helps with their business (and) automisation as well because a digital channel can assist in either signing up new products or assisting (support) into the branches.?

Among the other findings, Google also revealed that the search demand for financial products continues to climb up 11 per cent year-on-year, led by key market factors.

Google noticed a peak in searches following topical issues in May to June last year, such as an interest in the Euro following the Brexit announcement, the launch of Digibank by DBS Bank, and the launch of new retail bonds by Oxley and Hyflux.

The findings also showed that Singaporeans are savvy about their finances and keenly aware of movements in the financial sectors, Google noted. Two in three conduct their searches with a brand in mind when searching for financial products.

?Seventy per cent of Singaporeans turn to their smartphones when they need information about a local business compared to 43 per cent in the United States,? noted Mr Yue.

Using insights from Google?s Apac financial dashboard, financial brands can respond better to their consumers and engage them in more relevant ways, both online and offline, he said.</p>      
  </div>    
 </div>
 <div class="break"></div>
 <div class="container">
 <div id="box3">
      <h3 style=" padding :5px; color: #591a30; text-align: center;">Get insured today & Live stress free</h3>
      <img src="./image/sbi.png" align="top" width="150px"  height="50px" />
      <img src="./image/hdfc.png" align="top" width="150px"  height="50px" />
      <img src="./image/reliance.png" align="top" width="150px"  height="50px" />
      <img src="./image/apollo.png" align="top" width="150px"  height="50px" />
      <img src="./image/lic.png" align="top" width="150px"  height="50px" />
      <img src="./image/max.png" align="top" width="150px"  height="50px" />
      
    </div>
 </div>     
</body>
</html>