
<%@page import="java.sql.ResultSet"%>
<%@page import="mydb.InitDB"%>
<html>
<head>
	<title>Online Insurance Portal</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />	
        <link rel="stylesheet" type="text/css" href="css/common_header.css">
	<style type="text/css">
                
		#box1{
                        position: relative;
			float: left;
			display: block;			
			padding: 1%;
			margin: 10% 5% 0 2%;			
			width: 50%;
                        border: 6px solid black;			
		}
                
                #box1 li{
                        list-style-type: none;
                        text-align: right;
                }
                
                
                #box2{                        
			float: left;
			display: block;			
			padding: 1%;
			width: 100%;                        			
		}
                #box1 form {

                    text-align: left;
                    
                }
                
                .form-box table{
                    width: 100%;
                    table-layout: fixed;
                }
                .form-box table, th, td {
                    
                }
                #box1 form > label {
                    
                }
                #box1 form > input{
                    
                }


		.break {
			clear:both;
		}


	</style>
</head>
<body>
      <jsp:include page="menubar.jsp" />	  
  <div class="container">
     <div id="box1">
         <!-- Coding for what kind of policy to search User or Inventory ? -->
         <form name="searchtype" method="post" >
             <table>
                 <tr>
                     <td>
                         Searching for ?                         
                     </td>
                     <td>
                         <select name="audience" onchange="this.form.submit()">
                            <option value="none">Select</option>
                            <option value="inventory">Policy Inventory</option>
                            <option value="user">User Policy</option>                            
                        </select>
                     </td>
                 </tr>
             </table>
         </form>    
         <!-- END ///// -->
     <div>        
  </div>
         <div class="break"></div>
         

<%
 
 String audience = String.valueOf(request.getParameter("audience"));   
 if (audience.equalsIgnoreCase("inventory"))   {    
     out.println("<div class='form-box'><form name='policyinventory' method='post'");
     

     // select policy type ? term or life ?
     out.println("<table><tr><td>Policy Type &emsp;</td><td><select name='policytype' onchange=this.form.submit()><option value='none'> Select </option><option value='term'> Term </option><option value='ulip'> ULIP </option><option value='endowment'> Endowment </option></select>");
     out.println("</td></tr></table></form></div>");
 }else if (audience.equalsIgnoreCase("user"))   {
      response.sendRedirect("userpolicy_search.jsp");
 }else{    
 }

%>

 <!-- Coding what happens when user select 'term'  or 'blabla' insurance -->
<div class='form-box'><form name='terminsurance' method='post'>
<table><th>NAME</th><th>CATEGORY</th><th>SUMMARY</th>
<%
  String policytype = String.valueOf(request.getParameter("policytype"));
       
     String query="select policytype, policyname, policywording from policy_inventory where policytype="+"'"+policytype+"'";        
     InitDB obj=new InitDB();        
     ResultSet result= obj.selectQuery(query);
     while(result.next()){  
         String strpolicytype=result.getString("policytype");
         String strpolicyname=result.getString("policyname");
         String strpolicywording=result.getString("policywording");
         
%>

  <tr>      
      <td><input type="radio" name="tmppolicyname" value=<%=strpolicyname %> > <%=strpolicyname.toUpperCase() %> </input></td>
      <td><%=strpolicytype.toUpperCase() %></td>
      <td style='font-size: 0.5em;'><%=strpolicywording.substring(0, 100).toUpperCase() %></td>
  </tr>          
  
 <!-- END ///// -->
     
<%     
} 
result.close();
obj.killConnection();

%>
             
<%
    if (session.getAttribute("username") == null){        
        response.sendRedirect("./login.jsp");
    }
    
    %>      
    </table> 
    
    
    <input type="submit" name="editpolicy" value="Edit Policy"  onchange="this.form.submit()" />
    
    <% 
              String btneditpolicy=String.valueOf(request.getParameter("tmppolicyname"));   
                if(!btneditpolicy.equalsIgnoreCase("null")){
                    session.setAttribute("selectedpolicyname", btneditpolicy);
                    response.sendRedirect("policyinventory_edit.jsp");
                }                                              
    %>
        
  
    </form></div></body></html>
