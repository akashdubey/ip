<!DOCTYPE html>
<html>
<head>
	<title>Online Insurance Portal</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />	
        <link rel="stylesheet" type="text/css" href="css/common_header.css">
	<style type="text/css">
		
		#box1{
			float: left;
			display: block;			
			padding: 0;
			margin: 10% 5% 0 2%;						
			width: 25%;			
		}

		#box2{
			float: left;
			display: block;			
			padding: 0;
			margin: 10% 5% 0 15%;						
			width: 70%;			
                        height: 20%;
		}

		#box3{
			float: left;
			display: block;			
			padding: 0;
			margin: 10% 5% 0 2%;			
			background-color: grey;
			width: 25%;
		}

		.break {
			clear:both;
		}


	</style>
</head>
<body>
      <jsp:include page="menubar.jsp" />	  
 <div class="container">
     <div id="box2">
      <img src="./image/contactus.jpg" align="right" width="600px"  height="400px"/>
      <h3 style=" padding :5px; color: #591a30; ">Corporate Office Address</h3>
      <h4 style=" padding :5px; color: black; "> Plot No. 23/4, At. Post Bhoirwadi, Phase-III, Hinjewadi, Pune- 411057 </h4>
      <h3 style=" padding :5px; color: #591a30; ">Email </h3>
      <h4 style=" padding :5px; color: black; "> query@ipportal.com </h4>
      <h3 style=" padding :5px; color: #591a30; ">Toll Free line </h3>
      <h4 style=" padding :5px; color: black; "> 1800 254 5699</h4>      	
    </div>
 </div>
 
</body>
</html>