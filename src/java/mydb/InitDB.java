/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * Author : Akash Dubey
 */
package mydb;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 */
public class InitDB {
    public ResultSet resultset;
    public  Statement statement;
    public  PreparedStatement preparedStatement;
    public Connection connection;
    public boolean status=false;
    
    
    public InitDB(ResultSet resultset, Statement statement, PreparedStatement preparedStatement) {
        this.resultset = resultset;
        this.statement = statement;
        this.preparedStatement = preparedStatement;
        
    }
    
    public InitDB(PreparedStatement preparedstatement){
        this.preparedStatement=preparedstatement;
    }
    
    public InitDB() {
        try{
            String connectionUrl="jdbc:mysql://127.0.0.1:3306/maxinsurance";            
            String username="maxinsurance";
            String password="maxinsurance";
            Class.forName("com.mysql.jdbc.Driver");            
            connection = DriverManager.getConnection(connectionUrl,username,password);                        
        }catch(Exception e){
            e.printStackTrace();
        }
        
    }
    
    public ResultSet selectQuery(String query){
        try {
            statement=connection.createStatement();
            resultset=statement.executeQuery(query);                        
        } catch (Exception e) {
            e.printStackTrace();
        }
       return resultset;
    }
    
    public boolean insertQuery(String query){
        try {
            statement=connection.createStatement();
            int result=statement.executeUpdate(query);                        
            status=true;
        } catch (Exception e) {
            e.printStackTrace();
        }        
        return status;
    }
    
    public void killConnection(){
        try {
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
